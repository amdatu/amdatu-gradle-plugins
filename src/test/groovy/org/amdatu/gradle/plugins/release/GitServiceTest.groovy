/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.gradle.plugins.release

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.process.internal.ExecException
import groovy.util.GroovyTestCase
import groovy.mock.interceptor.*

/**
 * Test cases for GitService.
 */
class GitServiceTest extends GroovyTestCase {
	def File repoLocation
	def GitService service

	// tests that we can add an existing file to a Git repository.
	void testAddExistingFile() {
		File gitignore = new File(this.repoLocation, ".gitignore")
		gitignore << 'bin/*'

		assert this.service.add(gitignore) : "Failed to add existing file?!"
	}

	// tests that we cannot add a non-existing file to a Git repository.
	void testAddNonExistingFile() {
		assert !this.service.add(new File(this.repoLocation, "non-existing-file")) : "Succeeded into adding a non-existing file?!"
	}

	// tests that cleaning a Git repository removes all non-tracked files.
	void testClean() {
		File dummy = new File(this.repoLocation, "dummy")
		dummy << 'ignore me!'

		assert dummy.exists() : "Dummy file does not exist?!"
		assert !this.service.isTracked(dummy) : "Dummy file is tracked?!"

		assert this.service.clean(this.repoLocation, true /* wholeDir */, true /* force */) : "Failed to clean?!"
		assert !dummy.exists() : "Dummy file still does exist?!"
	}

	// tests that we can commit changes to a Git repository.
	void testCommit() {
		File dummy = new File(this.repoLocation, "testCommit.txt")
		dummy << 'test contents'

		assert this.service.hasLocalChanges() : "No local changes?!"

		assert this.service.add(dummy) : "Failed to add file?!"
		assert this.service.commit("test commit", false /* sign */) : "Failed to commit?!"

		assert !this.service.hasLocalChanges() : "Still local changes?!"
	}

	// tests that we can create a new tag on a repository.
	void testCreateTag() {
		File dummy = new File(this.repoLocation, "testTag.txt")
		dummy << 'test contents'

		assert this.service.add(dummy) : "Failed to add file?!"
		assert this.service.commit("test commit", false /* sign */) : "Failed to commit?!"

		assert this.service.tag('tag1', 'test-tag') : "Failed in creating a tag?!"
		assert !this.service.tag('tag1', 'other message') : "Succeeded into creating a duplicate tag?!"
		assert this.service.tag('other-tag', 'test-tag') : "Failed in creating a tag?!"

		assert this.service.latestReleaseTag("tag*") == 'tag1' : "Failed to determine latest tag?!"
		assert this.service.latestReleaseTag("other*") == 'other-tag' : "Failed to determine latest tag?!"
		assert this.service.latestReleaseTag("r*") == null : "Succeeded into getting non-existing tag?!"
	}

	// tests that we can get a configuration key
	void testGetExistingConfigKey() {
		assert this.service.getConfigOption('user.name') != null : "Failed to get configuration key?!"
	}

	// tests that we can get a configuration key
	void testGetNonExistingConfigKey() {
		assert this.service.getConfigOption('no-such-key-should-ever-exist') == null : "Succeeded in getting a non-existing configuration key?!"
	}

	protected void setUp() throws Exception {
		this.repoLocation = new File("/tmp/test-repo-${System.currentTimeMillis()}")

		"git init ${repoLocation.getAbsolutePath()}".execute()

		Project project = ProjectBuilder.builder().build()

		this.service = new GitService(project, this.repoLocation)
	}

	protected void tearDown() throws Exception {
		if (this.repoLocation.exists()) {
			this.repoLocation.deleteDir();
		}
	}
}
