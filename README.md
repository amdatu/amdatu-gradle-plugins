# Amdatu Gradle Plugins

Repository containing Gradle plugins for supporting the Amdatu Release process.  It
provides the following tasks:

- **stageRelease**: stages a new release of an Amdatu project;
- **cancelRelease**: cancels a previously staged release;
- **promoteRelease**: promotes a previously staged release;
- **checkBaseline**: verifies the current project(s) against the released
  baseline artifacts.

## Installation

The installation of the Amdatu Gradle plugins is relatively easy: all you need to do is
include the `amdatu-gradle-plugins` plugin on your build classpath and apply the various
plugins to your Gradle build. In most cases, this means you need to modify the
`build.gradle` file and add the following to the `buildscript` clause:

    // ...
    buildscript {
        repositories {
            ivy {
                url "https://bitbucket.org/amdatu/amdatu-gradle-plugins/raw/master/dist/"
            }
        }
        dependencies {
                classpath 'org.amdatu:amdatu-gradle-plugins:1.0.1'
                // ... any additional classpath entries should remain
        }
    }
    
And append the following:

    /* Load the Amdatu Baseline & Release plugins. */
    allprojects {
        apply plugin: org.amdatu.gradle.plugins.baseline.AmdatuBaselinePlugin
    }
    apply plugin: org.amdatu.gradle.plugins.release.AmdatuReleasePlugin
    /* Configure the release options; only when needed
    amdatuRelease {
        baseName = "my-amdatu-project"
        repoDir = "/path/to/amdatu-repository"
        exclude = "*.itest, *.demo"
    }
    */

The `AmdatuReleasePlugin` defines new tasks on the root project only and therefore does
not need to be applied to subprojects. It also has an additional script dependency aside
the Bnd script dependency: `org.osgi.impl.bundle.repoindex.cli.jar`, which is by default
included in the main Bnd library, so nothing needs to be done for that.

### Configuration

There are several configuration options by which we can tune the amdatu release plugin:

* `baseName`, defines the name of the project as used in the Amdatu repository. By
  default, the value equals to the project directory;
* `repoDir`, defines the location of the Amdatu repository. By default, this property is
  equal to `../amdatu-repository` (meaning that it is assumed to be placed next to your
  other Amdatu projects);
* `exclude`, defines what project names should be excluded from the release process. This
  allows you to exclude test and/or demo projects. By default, this property equals to
  `*.itest, *.demo` meaning that all projects ending in `.itest` and `.demo` are *not*
  released.

To override one or more of these configuration options, you can add them to your
`build.gradle` file as (append or change the existing configuration):

    // Configure Amdatu release plugin:
    amdatuRelease {
        repoDir = "/Users/amdatu/src/amdatu-repository"
        exclude = "*.itest, *.examples"
    }

## Releasing

The release of an Amdatu project is divided into two steps, described in more detail in
the following subsections. By default, the Amdatu release tasks make the following
assumptions:

1. tags are named according to the standard defined in the Amdatu Release Procedure (see
   [1]), that is, `r1`, `r2` and so on. By default, the next tag name is based on the last
   created tag (sorted chronologically). The tag name can be overridden by supplying the
   `tagName` environment property;
2. the project base name is the name of the directory in which the Amdatu project is
   checked out, which often equals the name of the repository as it appears on Bitbucket.
   The base name can be overridden by supplying the `baseName` environment property;
3. the location of Amdatu Repository is next to the to-be-released project, that is,
   "../amdatu-repository". This property can be overridden by specifying the `repoDir`
   environment property;
4. the binaries for all subprojects of the to-be-released project are included, except for
   subprojects ending in `.itest` and `.demo`. To override this behaviour, you can
   (re)define the `excludes` property (see example above);
5. the branch which is to be released is *master*. It is not possible to override this at
   the moment;
6. no other release for the same project is staged at the moment. The staging task will
   fail in case it detect an earlier staged release;
7. all used repositories are up-to-date and do not contain local changes;
8. a valid GPG key is present, either configured in Git directly, or using the GPGKEY
   environment variable.

### Staging the release

The staging of the release means that all release artifacts are prepared, collected and
staged into a staging directory inside the Amdatu Repository. The following release
artifacts are collected:

* an archive of all the sources in the project, including checksums;
* all bundles (JAR files) of the subprojects, for ease of use and verification.

For convenience, the bundles (JAR files) are represented in an OBR allowing one to
reference the staged binaries directly from a Bnd-compatible workspace.

To stage a release, you use the `stageRelease` task as defined on the root. For example:

    amdatu-web$ ./gradlew -DtagName=r5 -DbaseName=foo stageRelease
    :stageRelease
    
    Release foo-r5 successfully staged! You can send the vote email and await the results:
    - if the vote is successful, use 'promoteRelease' to promote the release;
    - if the vote is unsuccessful, use 'cancelRelease' to cancel the release.
    ...

Without any environment properties, the results would look like (assuming `r8` is the
current release of Amdatu-Web):

    amdatu-web$ ./gradlew stageRelease
    :stageRelease
    
    Release amdatu-web-r9 successfully staged! You can send the vote email and await the results:
    - if the vote is successful, use 'promoteRelease' to promote the release;
    - if the vote is unsuccessful, use 'cancelRelease' to cancel the release.
    ...

Note that under most circumstances supplying the tag name is *not* necessary.

Once the stageRelease task is finished, the following changes have been made:

* a tag is created in the source repository with the release tag name, for example, `r2`;
* a staging directory with the release artifacts is created and committed in Amdatu
  Repository, for example, `${AMDATU_REPO_HOME}/staging/amdatu-web-r2`;
* **both** the changes on the source repository as the Amdatu Repository are pushed to the
  remote repository on Bitbucket. This means that tag names are *not* reusable.


### Promoting or cancelling the release

Once a release has been staged for a project, the release vote can be started.  The result
of the vote can be positive meaning that the release can be promoted to release, or
negative meaning that the release is to be discarded.

#### Promoting the release

Promoting the release is done by running the `promoteRelease` task and uses the same
assumptions as the "stageRelease" task. This means that if you specified environment
properties for the "stageRelease" task, you *must* specify the same environment properties
for the "promoteTask" as well! For example:

    amdatu-web-master$ ./gradlew -DbaseName=amdatu-web stageRelease
    ...
    amdatu-web-master$ ./gradlew -DbaseName=amdatu-web promoteRelease
    :promoteRelease
    
    Release amdatu-web-r9 successfully promoted! Send the announcement email to
    notify the community about this.

The result of the promotion task is:

* the source archive of the released project, along with its checksums, is moved from the
  staging directory to `${AMDATU_REPO_HOME}/src-release`;
* the bundle binaries (JAR files) are moved from the staging directory to the release OBR
  (`${AMDATU_REPO_HOME}/release`);
* the release OBR's index is updated to include the newly added bundles;
* the staging directory itself is removed;
* a tag is created in the Amdatu repository to reflect the successful release of the
  Amdatu project;
* all changes are committed and pushed to the remote repository on Bitbucket.


#### Cancelling the release

Cancelling the release is done by running the `cancelRelease` task and removes the staging
directory from Amdatu Repository. It does *not* remove the tags created during the
"stageRelease" task. The removal of the staging directory is automatically pushed to the
remote repository. For example:

    amdatu-web-master$ ./gradlew -DbaseName=amdatu-web cancelRelease
    :cancelRelease
    
    Release amdatu-web-r9 is cancelled!

The result of the cancellation task is:

* the staging directory is removed from the Amdatu repository;
* all changes are committed and pushed to the remote repository on Bitbucket.


## Baselining

The baseline plugin provides functionality to verify whether changes made to subprojects
are properly reflected in their versioning using the semantic versioning rules.

**TODO**


## License

The Amdatu Gradle Plugins are licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).


## References

1. https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Release+Procedure

