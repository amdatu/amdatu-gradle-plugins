/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.gradle.plugins.release

import org.gradle.api.*
import org.gradle.api.logging.*
import org.gradle.tooling.*

import aQute.lib.deployer.FileRepo
import aQute.bnd.build.Workspace

import org.osgi.service.indexer.*
import org.osgi.service.indexer.impl.*

import groovy.json.JsonSlurper;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.FileVisitResult
import java.nio.file.SimpleFileVisitor
import java.util.Scanner;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.HeaderClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;

/**
 * Amdatu Release Plugin - provides tasks to support the Amdatu Release process.
 *
 * Should be applied from the master.gradle in the cnf/gradle directory, like:
 * <tt><code>apply from: rootProject.file('cnf/gradle/AmdatuReleasePlugin.gradle')</code></tt>
 *
 * Depends on biz.aQute.bnd &amp; org.osgi.impl.bundle.repoindex.cli as script dependencies.
 */
class AmdatuReleasePlugin implements Plugin<Project> {
    private String runningJVM;
    private String configuredJVM;

	void apply(Project project) {
		def extension = project.extensions.create('amdatuRelease', AmdatuReleasePluginExtension, project)
		def rootProjectName = project.getRootProject().name
                                
        this.runningJVM = JavaVersion.current().toString()
        this.configuredJVM = Workspace.getWorkspace(project.rootDir).getProperty("javac.target") 

		/**
		 * CANCEL RELEASE - cancels a staged release.
		 */
		project.tasks.create('cancelRelease') {
			description "Cancels a previously staged release of ${rootProjectName}"
			group 'release'

			doLast {
				def srcGit = new GitService(project)
                // AMDATUINF-9: do not bail out if local changes are found, but
                // be sure we do not talk to an out-of-date local/remote...
                if (srcGit.isLocalAheadOfRemote() || srcGit.isRemoteAheadOfLocal()) {
                    throw new RuntimeException("Source repository not in sync with remote!")
                }

				// Determine whether anything is staged at all...
				def tagName = srcGit.latestReleaseTag()
				def baseName = project.amdatuRelease.baseName
				def repoDir = project.amdatuRelease.repoDir

				def stageDirName = "staging/${baseName}-${tagName}"
				def stagingDir = new File(repoDir, stageDirName).getCanonicalFile()
				if (!stagingDir.exists() || !stagingDir.isDirectory()) {
					throw new RuntimeException("Nothing staged for ${baseName}!")
				}

				logger.info "Cancelling release ${baseName}-${tagName} in Amdatu-Repository..."

				def repoGit = new GitService(project, repoDir)
				repoGit.assertInSyncWithRemote()

				if (repoGit.isTracked(stageDirName)) {
					logger.info "Removing ${stageDirName} from Amdatu-Repository..."

					repoGit.rm(stageDirName, true /* recursive */, true /* force */)
					repoGit.commit("Cancelled release ${baseName}-${tagName}.")
					repoGit.pushAll()
				} else {
					logger.info "Removing untracked ${stageDirName} from Amdatu-Repository..."

					// Not in the Git repository, we can simply delete it...
					stagingDir.deleteDir()
				}

				if (stagingDir.exists()) {
					logger.warn "${stagingDir} still exists, not sure what went wrong?!"
				}

				println "\nRelease ${baseName}-${tagName} is cancelled!"
	
				Scanner scanner =  new Scanner(System.in)
				def mailTo
				while (!"y".equalsIgnoreCase(mailTo) && !"n".equalsIgnoreCase(mailTo)) {
					println "> Do you wish to open the default email client on this machine with a filled in template, with the possibility to enter details on the cancellation of this release?:  (y/n)"
					mailTo = scanner.nextLine();
				}
				if ("y".equalsIgnoreCase(mailTo)) {
					openMailClient(rootProjectName, null, baseName, tagName, ReleaseAction.CANCEL)
				}
			}
		}

		/**
		 * PROMOTE RELEASE - promotes a staged release to its final destination.
		 */
		project.tasks.create('promoteRelease') {
			description "Promotes a previously staged release of ${rootProjectName}"
			group 'release'

			doLast {
				// Expose the ant functionality to our tasks...
				ext.ant = project.ant

				def srcGit = new GitService(project)
				srcGit.assertInSyncWithRemote()

				// Determine whether anything is staged at all...
				def tagName = srcGit.latestReleaseTag()
				def baseName = project.amdatuRelease.baseName
				def repoDir = project.amdatuRelease.repoDir

				def stageDirName = "staging/${baseName}-${tagName}"
				def stagingDir = new File(repoDir, stageDirName).getCanonicalFile()
				if (!stagingDir.exists() || !stagingDir.isDirectory()) {
					throw new RuntimeException("Nothing staged for ${baseName}!")
				}

				def releaseDir = new File(repoDir, 'release').getCanonicalFile()
				if (!releaseDir.exists() || !releaseDir.isDirectory()) {
					throw new RuntimeException("No release directory in Amdatu-Repository?!")
				}

				def repoGit = new GitService(project, repoDir)
				repoGit.assertInSyncWithRemote()

				if (!repoGit.isTracked(stageDirName)) {
					throw new RuntimeException("Cannot promote ${baseName}-${tagName}: it is not in the Git repository!")
				}

				logger.info "Promoting release ${baseName}-${tagName} in Amdatu-Repository..."

				// Move the source archive to 'src-release'...
				def srcArchive = stageDirName + '/' + GitService.getSourceArchiveName(baseName, tagName)
				repoGit.mv("${srcArchive}", "src-release")
				repoGit.mv("${srcArchive}.md5", "src-release")
				repoGit.mv("${srcArchive}.sha-512", "src-release")

				// Move the individual bundles...
				project.fileTree(stagingDir)
						.include('**/*.jar')
						.each { File file ->
							def destDir = new File(releaseDir, file.getParentFile().getName())
							destDir.mkdir()
							repoGit.mv(file.getPath(), new File(destDir, file.getName()).getPath())
						}

				// Recreate the OBR index file(s) for the release repository...
				def resources = project.fileTree(releaseDir)
						.include('**/*.jar')
						.asType(Set.class)

				def (indexName, checksumName) = generateObrR5Index('Amdatu Release Repository', releaseDir, resources)
				ant.checksum file: "${releaseDir}/${indexName}", algorithm: 'sha-256', format: 'CHECKSUM', fileext: '.sha'
				repoGit.add(new File(releaseDir, indexName), new File(releaseDir, checksumName))

				repoGit.rm("${stageDirName}/", true /* recursive */, true /* force */)
				repoGit.clean("${stagingDir.getParent()}", true /* cleanWholeDirectory */, true /* force */)

				repoGit.commit("Promoted release ${baseName}-${tagName}.")
				repoGit.tag("${baseName}-${tagName}", "Released ${tagName} of ${baseName}.")

				repoGit.pushAll()
				repoGit.pushTags()

				if (stagingDir.exists()) {
					logger.warn "${stagingDir} still exists, not sure what went wrong?!"
				}

				println "\nRelease ${baseName}-${tagName} successfully promoted! Send the announcement email to notify the community about this.\n"
				
				Scanner scanner = new Scanner(System.in)
				def mailTo
				while (!"y".equalsIgnoreCase(mailTo) && !"n".equalsIgnoreCase(mailTo)) {
					println "> Do you wish to open the default email client on this machine with a filled in template containing all resolved issues matching this release in JIRA (if available)?:  (y/n)"
					mailTo = scanner.nextLine()
				}
				if ("y".equalsIgnoreCase(mailTo)) {
					openMailClient(rootProjectName, null, baseName, tagName, ReleaseAction.PROMOTE)
				}
			}
		}

		/**
		 * STAGE RELEASE - stages a new release for all projects.
		 */
		project.tasks.create('stageRelease') {
			description "Stages a release of ${rootProjectName} (both sources and binaries)"
			group 'release'

            doFirst {
                // AMDATU-INF10: fail build when compilers do not match...
                if (!this.runningJVM.equals(this.configuredJVM)) {
                    throw new RuntimeException("Cowardly refusing to release artifacts with incorrect Java version! Configured to ${configuredJVM} but running with ${runningJVM}. Please use the correct JDK (${configuredJVM}) for compiling and releasing!");
                }
            }

			doLast {
				// Expose the ant functionality to our tasks...
				ext.ant = project.ant

				def srcGit = new GitService(project)
				srcGit.assertInSyncWithRemote()

				def repoDir = project.amdatuRelease.repoDir
				def JIRAKey = project.amdatuRelease.JIRAKey
				def repoGit = new GitService(project, repoDir)
				def baseName = project.amdatuRelease.baseName
				def latestTagName = srcGit.latestReleaseTag()
				
				repoGit.assertInSyncWithRemote()

				// Sanity check: refuse to stage multiple releases for the same project...
				if (new File(repoDir, "staging/${baseName}-${latestTagName}").isDirectory()) {
					throw new RuntimeException("Release already staged for ${baseName}-${latestTagName}! Cancel it first before staging a new release!")
				}

				/* Runs a baseline build which clones the latest tag of the current project and baselines the source in it's current state against it
					throws an error when baselining fails, which means staging the release is canceled.
					Skipped when no prior tags exist (first release) */
				if (latestTagName != null) {
					runBaselineBuild(project, srcGit, latestTagName, logger)
				}

				// TODO this assumes the tags are always in ascending chronological order, which is sufficient for
				// now, but in case of patches on existing releases, this might not always be the case...
				def tagName = proposeTagName(latestTagName)

				def stagingDirName = "staging/${baseName}-${tagName}"
				def stagingDir = new File(repoDir, stagingDirName).getCanonicalFile()
				def stagingPath = stagingDir.getAbsolutePath()

				def repo = new FileRepo()
				repo.setProperties([ name: 'staging', location: stagingPath, readonly: 'false', latest: 'false' ]);
				repo.refresh()

				logger.info "Tagging release (${tagName})..."
				srcGit.tag(tagName, "Release ${tagName} of ${baseName}.")

				// Build all bundles and release them to our staging repository...
				logger.info "Building & releasing projects..."
				def bndWorkspace = getBndWorkspace(project)
				releaseProjectsToLocalRepository(bndWorkspace, repo, project, logger)
				
				logger.info "Creating source archive in ${stagingDirName}..."
				srcGit.archive(stagingPath, baseName, tagName)
				// Create checksums for the archive file...
				def srcArchive = stagingPath + '/' + GitService.getSourceArchiveName(baseName, tagName)
				ant.checksum file: "${srcArchive}", algorithm: 'sha-512', format: 'CHECKSUM'
				ant.checksum file: "${srcArchive}", algorithm: 'md5', format: 'CHECKSUM'

				def resources = project.fileTree(stagingPath)
						.include('**/*.jar')
						.asType(Set.class)
				def (indexName, checksumName) = generateObrR5Index("Staging repository for ${baseName}-${tagName}", stagingDir, resources)
				ant.checksum file: "${stagingPath}/${indexName}", algorithm: 'sha-256', format: 'CHECKSUM', fileext: '.sha'

				// Add & commit to actual repository...
				repoGit.addAll(stagingDirName)

				println "\nPrepared staging release ${baseName}-${tagName} to ${stagingPath} on your local machine! \nPlease check your local repositories whether you want to stage this release or not!\n"

				Scanner scanner = new Scanner(System.in);
				def push
				while (!push.equals("y") && !push.equals("n")) {
					println "> Do you wish to continue staging this release?:  (y/n)"
					push = scanner.nextLine();
				}
				if (push.equals("n")) {
					repoGit.rm("${stagingDirName}/", true /* recursive */, true /* force */)
					repoGit.clean("${stagingDir.getParent()}", true /* cleanWholeDirectory */, true /* force */)
					srcGit.removeTag(tagName)
					println "\nRemoved staging files from local release repository, and removed tag ${tagName} from project repository."
					println "\nRelease ${baseName}-${tagName} has not been staged, all changes have been reverted!"
				} else {
					repoGit.commit("Staging release ${baseName}-${tagName}.")
					srcGit.pushTags()
					repoGit.pushAll()

					// Explain the next steps...
					println "\nRelease ${baseName}-${tagName} successfully staged! You can send the vote email and await the results:"
					println "- if the vote is successful, use 'promoteRelease' to promote the release;"
					println "- if the vote is unsuccessful, use 'cancelRelease' to cancel the release.\n"

					def mailTo
					while (!"y".equalsIgnoreCase(mailTo) && !"n".equalsIgnoreCase(mailTo)) {
						println "> Do you wish to open the default email client on this machine with a filled in template containing all resolved issues matching this release in JIRA (if available)?:  (y/n)"
						mailTo = scanner.nextLine();
					}
					if ("y".equalsIgnoreCase(mailTo)) {
						openMailClient(rootProjectName, JIRAKey, baseName, tagName, ReleaseAction.STAGE)
					}
				}
			}
		}

		project.tasks.create('buildBaseline') {
			description "Run a baseline build against the last released tag"				
			group 'release'
			doLast {
				ext.ant = project.ant

				// Check out the latest tag to baseline against
				def repoGit = new GitService(project)
				def latestTagName = repoGit.latestReleaseTag()
				
				runBaselineBuild(project, repoGit, latestTagName, logger)
			}
		}
	}

	def runBaselineBuild(project, repoGit, latestTagName, logger) {
		def baselineWorkspaceDir = Files.createTempDirectory("arp").toFile()
		repoGit.cloneTag(latestTagName, baselineWorkspaceDir)

        // Run the baseline build itself...
		runGradleBuild(baselineWorkspaceDir, logger)

        // Create a bnd workspace from the cloned tag and release it's bundles to a temporary baseline repository
       	def tempBaselineRepo = new File(baselineWorkspaceDir, "baselinerepo")

       	def repo = new FileRepo()
		repo.setProperties([ name: 'baselining', location: tempBaselineRepo.getCanonicalPath(), readonly: 'false', latest: 'false' ])
		repo.refresh()

		def workspace = Workspace.getWorkspace(baselineWorkspaceDir)
        releaseProjectsToLocalRepository(workspace, repo, project, logger)
        repo.refresh()

        def repoFile = new File(project.rootDir, "cnf/ext/repositories.bnd")
        // We're going to revert these changes once we're done...
        Document repoFileBackup = new Document(repoFile.getText('UTF-8'))

        def bndBuildConfigFile = new File(project.rootDir, "cnf/build.bnd")
        // We do not want to persist our changes here, so keep the original document and restore it if needed...
        Document bndBuildConfigBackup = new Document(bndBuildConfigFile.getText('UTF-8'))
        def bndBuildConfigChanged = false

        try {
            // Add the temporary baseline repository to repositories.bnd
            addHeadersToBndFile("-baselinerepo", ["baseline"], repoFile)
            addHeadersToBndFile("-baseline", ["*"], repoFile)

            Attrs attrs = new Attrs()
            attrs.put("name", "baseline")
            attrs.put("locations", new File(tempBaselineRepo, "index.xml.gz").toURI().toASCIIString())

            HeaderClause clause = new HeaderClause("aQute.bnd.deployer.repository.FixedIndexedRepo", attrs)
            addHeaderClauseToBndFile(clause, repoFile)

            // Add headers which need to be excluded for baselining to build.bnd if not present...
            def headersToRemove = ["Git-Descriptor", "Git-SHA", "Bnd-LastModified", "Tool", "Created-By"]

            bndBuildConfigChanged = addHeadersToBndFile("-diffignore", headersToRemove, bndBuildConfigFile)
            if (bndBuildConfigChanged) {
                logger.debug("Added proper '-diffignore' clause to " + bndBuildConfigFile)
            }

            // Create Gradle project from current project and run 'build' task to check baseline errors
			runGradleBuild(project.rootDir, logger)
		} finally {
			baselineWorkspaceDir.deleteDir()
			// Revert repositories.bnd to the earlier created backup
			saveDocumentToFile(repoFileBackup, repoFile)

            if (bndBuildConfigChanged) {
                logger.debug("Reverting changes to " + bndBuildConfigFile)

                saveDocumentToFile(bndBuildConfigBackup, bndBuildConfigFile)
            }
		}
	}

	def Boolean addHeaderClauseToBndFile(HeaderClause clause, bndFile) {
		def bndEdited = false;

		BndEditModel editModel = new BndEditModel();
		editModel.loadFrom(bndFile)

		List<HeaderClause> plugins = editModel.getPlugins()
		if (!plugins.contains(clause)) {
			plugins.add(clause)
			editModel.setPlugins(plugins)
			bndEdited = true;
		}
		if (bndEdited) {
			Document document = new Document(bndFile.getText('UTF-8'))
			editModel.saveChangesTo(document)
			saveDocumentToFile(document, bndFile)
		}
		return bndEdited
	} 

	def Boolean addHeadersToBndFile(String headerKey, List<?> headerValues, bndFile) {
		def modelChanged = false

		BndEditModel editModel = new BndEditModel()
		editModel.loadFrom(bndFile)

		def existingHeaderValues = editModel.genericGet(headerKey)
		if (existingHeaderValues != null) {
			def existingHeaderValueList = existingHeaderValues.split(",").toList()
			existingHeaderValueList.addAll(headerValues)
			def valueSet = new HashSet(existingHeaderValueList)
			def finalHeaderList = new ArrayList<?>(valueSet)
            if (!existingHeaderValueList.equals(finalHeaderList)) {
			    editModel.genericSet(headerKey, finalHeaderList.join(","))
                modelChanged = true
            }
		} else {
			editModel.genericSet(headerKey, headerValues.join(", "))
			modelChanged = true;
		}

		if (modelChanged) {
			Document document = new Document(bndFile.getText('UTF-8'))
			editModel.saveChangesTo(document)
			saveDocumentToFile(document, bndFile)
		}

		return modelChanged
	}

	def saveDocumentToFile(Document document, File file) {
		def writer = file.newWriter()
		writer << document.get()
		writer.close()
	}

	def ProjectConnection getGradleConnectionForWorkspace(File workspace) {
		return GradleConnector.newConnector()
			.forProjectDirectory(workspace)
			.connect()
	}

	def runGradleBuild(workspace, logger) {
        logger.info("Running Gradle build for workspace: " + workspace)

        // Ignore any cached results, and pass the same log level to our build...
        def args = "--rerun-tasks"
        if (logger.isDebugEnabled()) {
            args += " --debug"
        } else if (logger.isInfoEnabled()) {
            args += " --info"
        } else if (logger.isQuietEnabled()) {
            args += " --quiet"
        }

		ProjectConnection connection = getGradleConnectionForWorkspace(workspace)
		try {
		    connection.newBuild()
              .withArguments(args.split(" "))
		      .forTasks("clean", "build")
		      .run()
		} finally {
            logger.info("Gradle build finished for workspace: " + workspace)

		    connection.close()
		}
	}

	def releaseProjectsToLocalRepository(workspace, repository, project, logger) {
		def repositoryName = repository.getName()
		def repositoryLocation = repository.getLocation()
		def excludes = project.amdatuRelease.exclude?.split("\\s*,\\s*").collect { patternToRegex(it) }
        workspace.refresh()
		workspace?.getAllProjects()
			.findAll { !it.isNoBundles() && !isExcluded(excludes, it) }
			.each {
				it.addBasicPlugin(repository)
				try {
					logger.info "Releasing bundles for ${it.getName()}..."
					// Release to local repository (e.g. cnf/releaserepo) using Bndtools Gradle plugin
					it.release(repositoryName, false /* test */)
				} finally {
					it.removeBasicPlugin(repository)
				}
			}
		// Delete -latest jars
		project.fileTree(repositoryLocation)
			.include('**/*-latest.jar')
			.each { File file ->
				logger.debug "Deleting redundant latest JAR: ${file.getName()}..."
				file.delete()
			}
		// Generate OBR index for new repo
		def resources = project.fileTree(repositoryLocation)
			.include('**/*.jar')
			.asType(Set.class)
		generateObrR5Index(repositoryName, Paths.get(repositoryLocation).toFile(), resources)
	}

    def boolean isExcluded(excludes, bndProject) {
        def name = bndProject.getName()
        excludes.find { name.find(it) } != null
    }

    /* converts a glob-pattern to a Java-regex. */
    def patternToRegex(pattern) {
        pattern.replaceAll('\\Q.\\E', '\\\\.').replaceAll('\\Q*\\E', '\\.\\*')
    }

	def Workspace getBndWorkspace(project) {
		try {
			return Workspace.getWorkspace(project.rootDir)
		} catch (IllegalArgumentException e) {
			return null
		}
	}

	def generateObrR5Index(String repoName, File dir, Set<File> resources, boolean compressed = true) {
		def config = [ 'root.url': dir.toURI().toString(), 'repository.name': repoName, 'pretty': Boolean.toString(!compressed), 'compressed': 'true' ]

		def indexFileName = compressed ? 'index.xml.gz' : 'index.xml'
		def indexFile = new File(dir, indexFileName)
		OutputStream output = new FileOutputStream(indexFile)
		try {
			new RepoIndex().index(resources, output, config)
		} finally {
			output.close()
		}

		[
			indexFileName,
			indexFileName + '.sha'
		]
	}

	def proposeTagName(String lastTagName) {
		def tagName = System.getProperty('tagName')
		if (tagName != null && !"".equals(tagName.trim())) {
			return tagName
		}
		def tagNo = 1
		if (lastTagName != null) {
			tagNo = lastTagName.substring(1).toInteger() + 1
		}
		String.format('r%d', tagNo)
	}

	def openMailClient(String rootProjectName, String JIRAKey, String baseName, String tagName, ReleaseAction action) {
		Desktop desktop;
		def subject = "[VOTE] Release Amdatu ${rootProjectName} ${tagName}"
		def body = "";
		if (ReleaseAction.PROMOTE.equals(action)) {
			subject = "[RESULT] " + subject
			body = "Hi all, \n\n The vote on releasing Amdatu Scheduling r2 has ended with the following results:"+
			"\n\n+1: <insert names>"+
			"\n\n-1: None"+
			"\n\nThe staged artifacts have been promoted to the main Amdatu Repository making them accessible for all users."+
			"\n\nThanks to all who have contributed and/or voted to and on this release."+
			"\n\n- The Amdatu team"
		} else if (ReleaseAction.CANCEL.equals(action)) {
			subject = "[CANCEL] " + subject
			body = "Hi all,\n\nThe vote on Amdatu ${rootProjectName} ${tagName} is cancelled due to the following reason(s):"+
			"\n\n<insert reason(s)>"+
			"\n\nWe will start a new vote once all issues are resolved."+
			"\n\n- The Amdatu team"
		} else {
			def issues = fetchIssuesForRelease(JIRAKey, tagName)
			def bugs = ""
			def improvements = ""
	  		issues?.each() { 
	  			if (it.fields.issuetype.name.equals("Bug")) {
	  				bugs += "[<https://amdatu.atlassian.net/browse/${it.key}/>] - ${it.fields.summary} \n"
	  			} else {
	  				improvements += "[<https://amdatu.atlassian.net/browse/${it.key}/>] - ${it.fields.summary} \n"
	  			}
	  		}
			body = "Hi all,\n\nWe would like to start a vote to release Amdatu ${rootProjectName} ${tagName}."
			if (!"".equals(bugs)) {
				body += "\n\nThis release solves the following bugs:\n${bugs}"
			}
			if (!"".equals(improvements)) {
				body += "\nIn addition, the following improvements were made:\n${improvements}"
			}
			body += 
				"\nTo test and verify the staged (binary) release artifacts, you can add the following staging repository to your \"repositories.bnd\" file:"+
				"\n\naQute.bnd.deployer.repository.FixedIndexedRepo;name=Amdatu ${rootProjectName} Staging;locations=http://repository.amdatu.org/staging/amdatu-${baseName}-${tagName}/index.xml.gz"+
				"\n\nIf you want to verify this release by its sources, you can extract the files from http://repository.amdatu.org/staging/amdatu-${baseName}-${tagName}/amdatu-${baseName}-src-${tagName}.tgz and build the project from scratch."+
				"\n\nPlease vote to approve or disapprove the release:"+
				"\n[ ] +1 Approve the release;"+
				"\n[ ] -1 Veto the release (please provide specific comments)."+
				"\n\nThe vote will be open for at least 72 hours."+
				"\n\n- The Amdatu team"
		}
		if (Desktop.isDesktopSupported() && (desktop = Desktop.getDesktop()).isSupported(Desktop.Action.MAIL)) {
			subject = URLEncoder.encode(subject)
			body = URLEncoder.encode(body) 
			// encode() encodes spaces as '+' which are literally taken over in certain mail clients
			subject = subject.replaceAll("\\+", "%20");
			body = body.replaceAll("\\+", "%20");
			URI mailto = new URI("mailto:dev@lists.amdatu.org?subject=${subject}&body=${body}")
			desktop.mail(mailto);
		} else {
			println "\nThis machine doesn't seem to support mailto unfortunately.. Instead, the following subject and message can be copied to send out a notification email:"
			println "\n${subject}"
			println "\n${body}"
		}
	}

	def fetchIssuesForRelease(String JIRAKey, String tagName) throws IOException {
		Scanner scanner = new Scanner(System.in);
		JsonSlurper jsonSlurper = new JsonSlurper()
	    try {
	    	if (JIRAKey == null) {
	    		println "\nNo matching 'JIRAKey' property corresponding to a JIRA project key could be found in the Amdatu Release configuration (located in master.gradle)"
				println "> If you know which key belongs to this release, you can enter it to automatically gather all matching issues for this release:"
				JIRAKey = scanner.nextLine();
	    	}
	    	new URL("https://amdatu.atlassian.net/rest/api/2/search?jql=project%20%3D%20${JIRAKey}%20AND%20fixVersion%20%3D%20${tagName}+order+by+key&fields=key,summary,issuetype").openStream().withStream { 
	  			new BufferedReader(new InputStreamReader(it)).withReader {
			        Object result = jsonSlurper.parse(it)
			        if (result != null) {
			            Map jsonResult = (Map) result
			            List issues = (List) jsonResult.get("issues")
			            if (issues.size() != 0) {
			            	return issues
			            }
			        }
			        return null
			    }
		    }
	    } catch (IOException e) {
        	println "\nJIRA responded with the following error"+ 
        	"\n\n   \"${e.getMessage()}\""+
        	"\n\nThis means either no project was found for the given key (${JIRAKey}), or no release was found with the given tag (${tagName}) for this project; hence no issues could be found for this release."+
        	"\nPlease make sure this release exists in JIRA!"
        	return null;
        }
	}

	enum ReleaseAction {
		STAGE, PROMOTE, CANCEL
	}

}

/* EOF */
