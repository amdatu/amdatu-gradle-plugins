/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.gradle.plugins.release

import org.gradle.api.Project
import org.gradle.process.internal.ExecException

/**
 * Provides simple access to Git, using the 'git' command installed on the local machine.
 * <p>
 * Based on https://github.com/ari/gradle-release-plugin/blob/master/src/main/groovy/au/com/ish/gradle/GitService.groovy
 * </p>
 */
class GitService {
	def Project project
	def String repoDir

	GitService(Project project) {
		this(project, project.rootDir.getAbsolutePath())
	}

	GitService(Project project, repoDir) {
		this.project = project
		this.repoDir = repoDir

		if (!new File(repoDir, ".git").isDirectory()) {
			throw new RuntimeException("Not a Git repository: ${repoDir}!")
		}
	}

	static def getSourceArchiveName(baseName, tagName) {
		return "${baseName}-src-${tagName}.tgz".toString()
	}

	def void assertInSyncWithRemote() {
		if (hasLocalChanges()) {
			throw new RuntimeException("Local changes found in ${repoDir}!")
		}
		if (isLocalAheadOfRemote()) {
			throw new RuntimeException("Local branch is ahead of remote for ${repoDir}!")
		}
		if (isRemoteAheadOfLocal()) {
			throw new RuntimeException("Remote branch is ahead of local for ${repoDir}!")
		}
	}

	def boolean isTracked(path) {
		gitExec(['ls-files', '--error-unmatch', path.toString()])[0] == 0
	}

	def boolean isLocalAheadOfRemote() {
		if (gitExec(['fetch'])[0] != 0) {
			project.getLogger().warn "No remote defined for ${repoDir}!"
			return false
		}
		gitExec(['status'])[1]?.contains('Your branch is ahead')
	}

	def boolean isRemoteAheadOfLocal() {
		if (gitExec(['fetch'])[0] != 0) {
			project.getLogger().warn "No remote defined for ${repoDir}!"
			return false
		}
		gitExec(['status'])[1]?.contains('Your branch is behind')
	}

	def boolean hasLocalChanges() {
		def out = gitExec(['status', '--porcelain'])[1]
		out ? (out.contains('M ') || out.contains('?? ')) : false
	}

	def String latestReleaseTag(glob = 'r*') {
		gitExec(['describe', '--tags', '--abbrev=0', "--match=${glob}"])[1]?.trim()
	}

	def add(File... files) {
		def args = ['add']
		files.each {
			args.add(it.getPath())
		}

		gitExec(args)[0] == 0
	}

	def addAll(dir) {
		gitExec(['add', '-A', dir.toString()])[0] == 0
	}

	def clean(dir, cleanWholeDir = false, force = false) {
		def args = ['clean']
		if (cleanWholeDir) {
			args.add('-d')
		}
		if (force) {
			args.add('-f')
		}
		args.add(dir.toString())
		
		gitExec(args)[0] == 0
	}

	def commit(message, sign = true) {
		gitExec(['commit', sign ? getCommitSigningOptions() : [], '-m', message].flatten())[0] == 0
	}

	def pushAll() {
		gitExec(['push', '--all'])[0] == 0
	}

	def pushTags() {
		gitExec(['push', '--tags'])[0] == 0
	}

	def mv(src, dest, force = false) {
		def args = ['mv']
		if (force) {
			args.add('-f')
		}
		args.add(src.toString())
		args.add(dest.toString())

		gitExec(args)[0] == 0
	}

	def rm(dir, recursive = false, force = false) {
		def args = ['rm']
		if (recursive) {
			args.add('-r')
		}
		if (force) {
			args.add('-f')
		}
		args.add(dir.toString())

		gitExec(args)[0] == 0
	}

	def tag(tagName, message) {
		gitExec(['tag', getTagSigningOptions(), '-m', message, tagName].flatten())[0] == 0
	}

	def archive(dir, prefix, tagName) {
		def srcArchiveName = GitService.getSourceArchiveName(prefix, tagName)
		gitExec(['archive', '--format=tgz', "--prefix=${prefix}/", "--output=${dir}/${srcArchiveName}", tagName.toString()])[0] == 0
	}

	def removeTag(tagName) {
		gitExec(['tag', '-d', tagName.toString()])[0] == 0
	}

	def cloneTag(tagName, targetDir) {
		gitExec(['clone', '-b', tagName.toString(), '.', targetDir.toString()])[0] == 0
	}
	
	def protected getConfigOption(key) {
        // this fails with a non-zero exit value when no such configuration value exists
		def (exitValue, val) = gitExec(['config', key])
		if (exitValue == 1) {
			// no such key, check whether we can get it globally
			(exitValue, val) = gitExec(['config', '--global', key])
		}
		return val
	}

	/**
	 * Tries to add the right options to create signed commits.
	 */
	def protected getCommitSigningOptions() {
		def key = getConfigOption('user.signingkey')
		if (key == null) {
			key = System.getProperty('GPGKEY')
		}
		if (key != null) {
			return ["--gpg-sign=$key"]
		} else {
			return []
		}
	}

	/**
	 * Tries to add the right option to create signed tags.
	 */
	def protected getTagSigningOptions() {
		def key = getConfigOption('user.signingkey')
		if (key != null) {
			return ['-s']
		} else {
			key = System.getProperty('GPGKEY')
			if (key != null) {
				return ['-u', key.trim()]
			} else {
				return ['-a']
			}
		}
	}

	def protected gitExec(List _args) {
		def stderr = new ByteArrayOutputStream()
		def stdout = new ByteArrayOutputStream()

		def result = project.exec {
			executable = 'git'
			args = _args
			workingDir = this.repoDir
			standardOutput = stdout
			errorOutput = stderr
			ignoreExitValue = true /* we handle this ourselves */
		}

		if (result.getExitValue() != 0) {
			project.getLogger().debug(stderr.toString().trim() ?: "<no error output>")
		}
		
		def out = stdout.toString().trim()
		[result.getExitValue(), "".equals(out) ? null : out]
	}
}

/* EOF */
