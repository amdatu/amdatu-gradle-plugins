/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.gradle.plugins.release

import aQute.bnd.build.Workspace

import org.gradle.api.Project

class AmdatuReleasePluginExtension {
	private final Project project

    /* the directory where Amdatu-Repository is checked out */
	String repoDir
    /* the base name to use for the source release artifact(s) */
	String baseName
    /* the (optional) list of project(name)s to exclude */
    String exclude
    /* the (optional) JIRA key which corresponds to this project in JIRA*/
    String JIRAKey
	
	AmdatuReleasePluginExtension(Project project) {
		this.project = project
		this.repoDir = getAmdatuRepository(project)
		this.baseName = getProjectBaseName(project)
        this.exclude = "*.itest, *.demo"
        this.JIRAKey = null
	}

	private static String getProjectBaseName(Project project) {
		def baseName = System.getProperty('baseName')
		if (baseName != null && !"".equals(baseName.trim())) {
			return baseName
		}
		project.getRootProject().name
	}

	private static String getAmdatuRepository(Project project) {
		def amdatuRepoDir = System.getProperty('repoDir')
		if (amdatuRepoDir == null) {
			// Assume the repository is in our parent directory...
			amdatuRepoDir = project.getRootProject().file('../amdatu-repository')
		}
		amdatuRepoDir.getCanonicalPath()
	}
}

/* EOF */
