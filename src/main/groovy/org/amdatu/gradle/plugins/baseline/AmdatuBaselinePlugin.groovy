/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.gradle.plugins.baseline

import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.InvalidUserDataException
import org.gradle.api.tasks.TaskValidationException

import aQute.bnd.differ.Baseline
import aQute.bnd.differ.Baseline.BundleInfo
import aQute.bnd.differ.Baseline.Info
import aQute.bnd.differ.DiffPluginImpl
import aQute.bnd.build.ProjectBuilder
import aQute.bnd.build.Workspace
import aQute.bnd.osgi.Instructions
import aQute.bnd.osgi.Jar
import aQute.bnd.service.diff.Delta
import aQute.bnd.version.Version
import aQute.service.reporter.Report.Location
import aQute.service.reporter.Reporter
import aQute.service.reporter.Reporter.SetLocation

/**
 * Amdatu Baseline Plugin - provides tasks to support baselining using Bnd.
 *
 * Invokes baselining of a project against the latest released version (in releaserepo) without the need for
 * explicitly enabling the baseline option in Bnd's main configuration. 
 *
 * Should be applied from the master.gradle in the cnf/gradle directory, like:
 * <pre>allprojects { apply plugin: org.amdatu.gradle.plugins.baseline.AmdatuBaselinePlugin }</pre>
 *
 * Depends on biz.aQute.bnd as script dependency.
 */
class AmdatuBaselinePlugin implements Plugin<Project> {
	void apply(Project project) {
		/**
		 * Invokes baselining of a project against the latest released version (in releaserepo) without 
		 * the need for explicitly enabling the baseline option in Bnd's main configuration. Reports all
		 * found errors at once (that is, after all sub-bundles have been checked). 
		 */
		project.tasks.create('checkBaseline') {
			description "Runs the baseline analysis on this project and all projects it depends on."
			group 'verification'

			onlyIf {
				def bndProject = getBndProject(project)
				bndProject != null && !bndProject.isNoBundles()
			}

			doLast {
				def bndProject = getBndProject(project)
				def failures = []

				bndProject?.getSubBuilders().each {
					def older = it.getLastRevision();
					if (older == null) {
						logger.info "* Cannot baseline ${it.getBsn()}: no release available for baselining!"
					} else {
						def newer = it.build()

						def result = analyseBaseline(bndProject, older, newer);
						if (result && !result[0].mismatch) {
							logger.info "* No baseline issues found for ${it.getBsn()}."
						} else {
							failures += new InvalidUserDataException(dumpAnalysisResults(result))
						}
					}
				}

				if (!failures.isEmpty()) {
					throw new TaskValidationException("Baseline errors found for ${project.name}:", failures)
				}
			}
		}
	}

	def getBndProject(project) {
		try {
			return Workspace.getProject(project.projectDir)
		} catch (IllegalArgumentException e) {
			return null
		}
	}

	def analyseBaseline(reporter, older, newer) {
		def differ = new DiffPluginImpl()
		def baseline = new Baseline(reporter, differ)

		def infos = baseline.baseline(newer, older, null).sort { a, b -> a.packageName <=> b.packageName }
		return [
			baseline.getBundleInfo(),
			infos == null ? []: infos.findAll { it.packageDiff.getDelta() != Delta.UNCHANGED }
		]
	}

	def dumpAnalysisResults(bundleInfo, infos) {
		StringBuilder sb = new StringBuilder()
		sb.append String.format("Bundle '%s' v%s should be at least v%s!", bundleInfo.bsn, bundleInfo.version, bundleInfo.suggestedVersion)

		// determine the max length of all package names for nice formatting results; note that we've
		// already sorted the infos in ascending order
		def len = 2 + (infos.isEmpty() ? 0 : infos.last().packageName.length())
		def fmt = "  %-${len}s %-10s %-10s %-10s %-10s %-10s%n"

		infos.each {
			if (it == infos.first()) {
				sb.append "\nThe following package(s) have version mismatches:\n"
				sb.append String.format(fmt, "Package", "Delta", "Old", "New", "Suggested", "If Prov.");
			}
			sb.append String.format(fmt,
					it.packageName, //
					it.packageDiff.getDelta(), //
					it.olderVersion.equals(Version.LOWEST) ? "-" : it.olderVersion, //
					it.newerVersion, //
					it.suggestedVersion.compareTo(it.newerVersion) <= 0 ? "= OK" : it.suggestedVersion, //
					it.suggestedIfProviders == null ? "-" : it.suggestedIfProviders);
		}
		return sb.toString();
	}
}

/* EOF */
